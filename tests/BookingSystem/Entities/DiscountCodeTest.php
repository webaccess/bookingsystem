<?php

class DiscountCodeTest extends PHPUnit_Framework_TestCase
{

    public function testConstruct()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $this->assertInstanceOf('\BookingSystem\Entities\DiscountCode', $discountCode);
    }

    public function testGetters()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setId(3);
        $this->assertEquals(3, $discountCode->getId());
    }

    public function testSetPercentage()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setPercentage(0);
        
        $this->assertEquals(0, $discountCode->getDiscountAmount(150));
    }

    public function testSetPercentage2()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setPercentage(100);
        
        $this->assertEquals(150, $discountCode->getDiscountAmount(150));
    }

    public function testSetPercentage3()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setPercentage(33);
        
        $this->assertEquals(50, $discountCode->getDiscountAmount(150));
    }

    public function testSetPercentage4()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setPercentage(-150);
        
        $this->assertEquals(0, $discountCode->getDiscountAmount(150));
    }

    public function testSetPercentage5()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setPercentage(50);
        
        $this->assertEquals(25, $discountCode->getDiscountAmount(50));
        $this->assertEquals(50, $discountCode->getPercentage());
    }

    public function testSetUsed()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setUsed(true);

        $this->assertEquals(true, $discountCode->getUsed());
    }

    public function testGenerateCode()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setCode(\BookingSystem\Entities\DiscountCode::generateCode());

        $this->assertEquals(8, strlen($discountCode->getCode()));
    }

    public function testGetExpirationDate()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setExpirationDate(new \DateTime('2013-01-15'));

        $this->assertEquals('15/01/2013', $discountCode->getExpirationDate()->format('d/m/Y'));
    }

    public function testCheckExpirationDate()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setExpirationDate(new \DateTime('2013-01-15'));

        $this->assertEquals(false, $discountCode->checkExpirationDate());
    }

    public function testCheckExpirationDate2()
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setExpirationDate(new \DateTime('2015-01-01'));

        $this->assertEquals(true, $discountCode->checkExpirationDate());
    }

}