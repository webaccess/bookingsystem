<?php

class RoomTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $room = new \BookingSystem\Entities\Room();
        
        $this->assertInstanceOf('\BookingSystem\Entities\Room', $room);
    }

    public function testGetters()
    {
        $room = new \BookingSystem\Entities\Room();
        $room->setId(3);
        $room->setType(new \BookingSystem\Entities\RoomType());
        
        $this->assertEquals(3, $room->getId());
        $this->assertInstanceOf('\BookingSystem\Entities\RoomType', $room->getType());
    }
}