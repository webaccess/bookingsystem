<?php

class CustomerTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $customer = new \BookingSystem\Entities\Customer();
        $this->assertInstanceOf('\BookingSystem\Entities\Customer', $customer);
    }

    public function testGetters()
    {
        $customer = new \BookingSystem\Entities\Customer();
        $customer->setId(1);
        $customer->setLastName('Doe');
        $customer->setFirstName('John');
        $customer->setEmail('john.doe@gmail.com');
        $customer->setPhone('0123456789');

        $this->assertEquals(1, $customer->getId());
        $this->assertEquals('Doe', $customer->getLastName());
        $this->assertEquals('John', $customer->getFirstName());
        $this->assertEquals('john.doe@gmail.com', $customer->getEmail());
        $this->assertEquals('0123456789', $customer->getPhone());
    }
}
