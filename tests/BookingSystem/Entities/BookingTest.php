<?php

class BookingTest extends PHPUnit_Framework_TestCase
{
    public function testConstruct()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $this->assertInstanceOf('\BookingSystem\Entities\Booking', $booking);
    }

    public function testGetters()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $booking->setId(3);
        $this->assertEquals(3, $booking->getId());

        $room = new \BookingSystem\Entities\Room();
        $booking->setRoom($room);
        $this->assertEquals($room, $booking->getRoom());

        $startDate = new DateTime('2014-01-15');
        $booking->setStartDate($startDate);
        $this->assertEquals($startDate, $booking->getStartDate());

        $endDate = new DateTime('2014-01-15');
        $booking->setEndDate($endDate);
        $this->assertEquals($endDate, $booking->getEndDate());

        $booking->setAdultsNumber(3);
        $this->assertEquals(3, $booking->getAdultsNumber());
        $booking->setChildrenNumber(2);
        $this->assertEquals(2, $booking->getChildrenNumber());
    }

    public function testGetNightsNumber()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $booking->setStartDate(new \DateTime('2014-01-28'));
        $booking->setEndDate(new \DateTime('2014-01-28'));

        $this->assertEquals(1, $booking->getNightsNumber());
    }

    public function testGetNightsNumber2()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $booking->setStartDate(new \DateTime('2014-01-28'));
        $booking->setEndDate(new \DateTime('2014-01-31'));

        $this->assertEquals(4, $booking->getNightsNumber());
    }

    public function testGetNightsNumber3()
    {
        $this->setExpectedException('Exception');
        $booking = new \BookingSystem\Entities\Booking();
        $booking->setStartDate(new \DateTime('2014-01-28'));

        $this->assertEquals(false, $booking->getNightsNumber());
    }

    public function testCheckAdultsNumber()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxAdultsNumber(2);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking->setRoom($room);
        $booking->setAdultsNumber(2);

        $this->assertEquals(true, $booking->checkAdultsNumber());
    }

    public function testCheckAdultsNumberMax()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxAdultsNumber(2);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking->setRoom($room);
        $booking->setAdultsNumber(3);

        $this->assertEquals(false, $booking->checkAdultsNumber());
    }

    public function testCheckChildrenNumber()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxChildrenNumber(2);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);
        
        $booking->setRoom($room);
        $booking->setChildrenNumber(2);

        $this->assertEquals(true, $booking->checkChildrenNumber());
    }

    public function testCheckChildrenNumberMax()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxChildrenNumber(3);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking->setRoom($room);
        $booking->setChildrenNumber(4);

        $this->assertEquals(false, $booking->checkChildrenNumber());
    }

    public function testGetTotal()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setPrice(150);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking->setRoom($room);
        $booking->setStartDate(new \DateTime('2014-01-28'));
        $booking->setEndDate(new \DateTime('2014-01-31'));

        $this->assertEquals(150 * 4, $booking->getTotal());
    }

    public function testGetTotal2()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $booking->setStartDate(new \DateTime('2014-01-28'));
        $booking->setEndDate(new \DateTime('2014-01-31'));

        $this->assertEquals(0, $booking->getTotal());
    }

    public function testSetDiscountCode()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setPrice(150);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking->setRoom($room);
        $booking->setStartDate(new \DateTime('2014-01-28'));
        $booking->setEndDate(new \DateTime('2014-01-31'));

        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setPercentage(100);
        $booking->setDiscountCode($discountCode);

        $this->assertEquals(0, $booking->getTotal());
    }

    public function testSetCustomer()
    {
        $booking = new \BookingSystem\Entities\Booking();
        $customer = new \BookingSystem\Entities\Customer();

        $booking->setCustomer($customer);

        $this->assertInstanceOf('\BookingSystem\Entities\Customer', $booking->getCustomer());
    }
}