<?php

class OrderTest extends PHPUnit_Framework_TestCase
{

    public function testConstruct()
    {
        $order = new \BookingSystem\Entities\Order();
        
        $this->assertInstanceOf('\BookingSystem\Entities\Order', $order);
    }

    public function testGetters()
    {
        $order = new \BookingSystem\Entities\Order();
        $order->setId(3);
        
        $this->assertEquals(3, $order->getId());
    }


    public function testAddBooking()
    {
        $order = new \BookingSystem\Entities\Order();
        $booking = new \BookingSystem\Entities\Booking();
        $order->addBooking($booking);

        $this->assertEquals(1, sizeof($order->getBookings()));
    }

    public function testGetTotal()
    {
        $order = new \BookingSystem\Entities\Order();
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setPrice(150);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking->setRoom($room);
        $booking->setStartDate(new \DateTime('2014-01-28'));
        $booking->setEndDate(new \DateTime('2014-01-31'));
        $order->addBooking($booking);

        $this->assertEquals(150 * 4, $order->getTotal());
    }

    public function testGetTotal2()
    {
        $order = new \BookingSystem\Entities\Order();
        $booking = new \BookingSystem\Entities\Booking();
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setPrice(150);

        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking->setRoom($room);
        $booking->setStartDate(new \DateTime('2014-01-28'));
        $booking->setEndDate(new \DateTime('2014-01-31'));
        $order->addBooking($booking);

        $booking2 = new \BookingSystem\Entities\Booking();
        $roomType2 = new \BookingSystem\Entities\RoomType();
        $roomType2->setPrice(350);
        
        $room2 = new \BookingSystem\Entities\Room();
        $room2->setType($roomType2);

        $booking2->setRoom($room2);
        $booking2->setStartDate(new \DateTime('2014-02-01'));
        $booking2->setEndDate(new \DateTime('2014-02-09'));
        $order->addBooking($booking2);

        $this->assertEquals(150 * 4 + 350 * 9, $order->getTotal());
    }

}