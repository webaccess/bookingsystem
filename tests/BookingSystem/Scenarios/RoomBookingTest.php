<?php

class RoomBookingTest extends PHPUnit_Framework_TestCase
{

    public function testConstruct()
    {
        $roomBooking = new \BookingSystem\Scenarios\RoomBooking();
        $this->assertInstanceOf('\BookingSystem\Scenarios\RoomBooking', $roomBooking);
    }

    public function testBook()
    {
        $roomBooking = new \BookingSystem\Scenarios\RoomBooking();

        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxAdultsNumber(3);
        $roomType->setMaxChildrenNumber(3);
        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking = new \BookingSystem\Entities\Booking();

        $room->setAvailable(true);
        $booking->setRoom($room);

        $booking->setAdultsNumber(2);
        $booking->setChildrenNumber(2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::BOOKING_VALIDATED, $roomBooking->book($booking));
    }

    public function testBook2()
    {
        $roomBooking = new \BookingSystem\Scenarios\RoomBooking();

        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxAdultsNumber(3);
        $roomType->setMaxChildrenNumber(3);
        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking = new \BookingSystem\Entities\Booking();

        $room->setAvailable(false);
        $booking->setRoom($room);

        $booking->setAdultsNumber(2);
        $booking->setChildrenNumber(2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_NOT_AVAILABLE, $roomBooking->book($booking));
    }

    public function testBook3()
    {
        $roomBooking = new \BookingSystem\Scenarios\RoomBooking();

        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxAdultsNumber(1);
        $roomType->setMaxChildrenNumber(2);
        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking = new \BookingSystem\Entities\Booking();

        $booking->setRoom($room);

        $booking->setAdultsNumber(2);
        $booking->setChildrenNumber(2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_MAX_ADULTS, $roomBooking->book($booking));
    }

    public function testBook4()
    {
        $roomBooking = new \BookingSystem\Scenarios\RoomBooking();

        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setMaxAdultsNumber(2);
        $roomType->setMaxChildrenNumber(1);
        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);

        $booking = new \BookingSystem\Entities\Booking();

        $booking->setRoom($room);

        $booking->setAdultsNumber(2);
        $booking->setChildrenNumber(2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_MAX_CHILDREN, $roomBooking->book($booking));
    }
}