<?php

class CustomerManagerTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $adapter = new \Database\PDOAdapter(DB_DRIVER . ':dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
        $this->customerMapper = new \BookingSystem\Mappers\Database\CustomerMapper($adapter);
    }

    public function tearDown()
    {
        $this->flushCustomers();
    }

    public function testConstruct()
    {
        $customerManager = new \BookingSystem\Services\CustomerManager($this->customerMapper);
        $this->assertInstanceOf('\BookingSystem\Services\CustomerManager', $customerManager);
    }

    public function testGetCustomer()
    {
        $customerManager = new \BookingSystem\Services\CustomerManager($this->customerMapper);
        $customer = $customerManager->getCustomer('Doe', 'John', 'john.doe@gmail.com', '0123456789');

        $this->assertInstanceOf('\BookingSystem\Entities\Customer', $customer);
        $this->assertEquals('Doe', $customer->getLastName());
        $this->assertEquals('John', $customer->getFirstName());
        $this->assertEquals('john.doe@gmail.com', $customer->getEmail());
        $this->assertEquals('0123456789', $customer->getPhone());
    }

    public function testInsertCustomer()
    {
        $customerManager = new \BookingSystem\Services\CustomerManager($this->customerMapper);
        $customer = $customerManager->getCustomer('Doe', 'John', 'john.doe@gmail.com', '0123456789');

        $this->assertEquals(sizeof($this->customerMapper->findAll()), 0);

        $customerManager->insert($customer);
        
        $this->assertEquals(sizeof($this->customerMapper->findAll()), 1);
    }

    public function flushCustomers()
    {
        $rooms = $this->customerMapper->truncate();
    }
}