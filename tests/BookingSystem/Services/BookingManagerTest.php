<?php

class BookingManagerTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $adapter = new \Database\PDOAdapter(DB_DRIVER . ':dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
        $this->roomTypeMapper = new \BookingSystem\Mappers\Database\RoomTypeMapper($adapter);
        $this->roomMapper = new \BookingSystem\Mappers\Database\RoomMapper($adapter, $this->roomTypeMapper);
        $this->discountCodeMapper = new \BookingSystem\Mappers\Database\DiscountCodeMapper($adapter);
        $this->customerMapper = new \BookingSystem\Mappers\Database\CustomerMapper($adapter);
        $this->bookingMapper = new \BookingSystem\Mappers\Database\BookingMapper($adapter, $this->roomMapper, $this->customerMapper, $this->discountCodeMapper);

        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setPrice(150);
        $roomType->setMaxAdultsNumber(2);
        $roomType->setMaxChildrenNumber(2);
        $this->roomTypeMapper->insert($roomType);

        $roomType2 = new \BookingSystem\Entities\RoomType();
        $roomType2->setPrice(350);
        $roomType2->setMaxAdultsNumber(2);
        $roomType2->setMaxChildrenNumber(2);
        $this->roomTypeMapper->insert($roomType2);

        $roomManager = new \BookingSystem\Services\RoomManager($this->roomMapper, $this->roomTypeMapper);
        $roomManager->create(1);
        $roomManager->create(2);
        $roomManager->create(2);
    }

    public function tearDown()
    {
        $this->flushBookings();
        $this->flushRooms();
    }

    public function testConstruct()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $this->assertInstanceOf('\BookingSystem\Services\BookingManager', $bookingManager);
    }

    public function testBook()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $code = $bookingManager->canBook(1, '2014-02-10', '2014-02-16', 2, 2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::BOOKING_VALIDATED, $code);
    }

    public function testBookWithExistingBookingDuringPeriod()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $bookingManager->book(1, '2014-02-10', '2014-02-12', 2, 2);
        $bookingManager->book(2, '2014-02-10', '2014-02-12', 2, 2);

        $code = $bookingManager->canBook(1, '2014-02-10', '2014-02-16', 2, 2);
        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_NOT_AVAILABLE, $code);
    }

    public function testBookWithExistingBookingDuringPeriod2()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $bookingManager->book(1, '2014-02-10', '2014-02-12', 2, 2);
        $bookingManager->book(2, '2014-02-08', '2014-02-12', 2, 2);
        $code = $bookingManager->canBook(1, '2014-02-10', '2014-02-16', 2, 2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_NOT_AVAILABLE, $code);
    }
    
    public function testBookWithExistingBookingDuringPeriod3()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $bookingManager->book(1, '2014-02-10', '2014-02-12', 2, 2);
        $bookingManager->book(2, '2014-02-15', '2014-02-20', 2, 2);
        $bookingManager->book(2, '2014-02-15', '2014-02-20', 2, 2);
        $code = $bookingManager->canBook(2, '2014-02-10', '2014-02-16', 2, 2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_NOT_AVAILABLE, $code);
    }

    public function testBookWithExistingBookingDuringPeriod4()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $bookingManager->book(1, '2014-02-10', '2014-02-20', 2, 2);

        $code = $bookingManager->canBook(1, '2014-02-12', '2014-02-13', 2, 2);

        $this->assertEquals(\BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_NOT_AVAILABLE, $code);
    }

    public function testFindBookings()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $bookingManager->book(1, '2014-02-10', '2014-02-12', 2, 2);
        $bookingManager->book(2, '2014-02-10', '2014-02-16', 2, 2);

        $this->assertEquals(1, sizeof($bookingManager->findBookings(1)));
    }

    public function testBookWithInvalidRoomType()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $code = $bookingManager->book(4, '2014-02-10', '2014-02-16', 2, 2);

        $this->assertEquals(false, $code);
    }

    public function testFindAllBookings()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $bookingManager->book(1, '2014-02-10', '2014-02-12', 2, 2);
        $bookingManager->book(2, '2014-02-15', '2014-02-20', 2, 2);
        $bookingManager->book(2, '2014-02-21', '2014-02-24', 2, 2);

        $this->assertEquals(3, sizeof($bookingManager->findBookings()));
    }

    public function testFindDiscountcode()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setCode('ABCDE');
        $this->discountCodeMapper->insert($discountCode);

        $this->assertInstanceOf('\BookingSystem\Entities\DiscountCode', $bookingManager->findDiscountCode('ABCDE'));
    }

    public function testFindUnvalidDiscountcode()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setCode('BCDEF');
        $this->discountCodeMapper->insert($discountCode);

        $this->assertEquals(false, $bookingManager->findDiscountCode('ABCDE'));
    }

    public function testGetBooking()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);
        
        $this->assertInstanceOf('\BookingSystem\Entities\Booking', $bookingManager->getBooking(1, '2014-02-10', '2014-02-12', 2, 2));
    }

    public function testGetUnvalidBooking()
    {
        $bookingManager = new \BookingSystem\Services\BookingManager($this->roomMapper, $this->bookingMapper, $this->discountCodeMapper);

        $this->assertEquals(false, $bookingManager->getBooking(11, '2014-02-10', '2014-02-12', 2, 2));
    }

    public function flushBookings()
    {
        $this->bookingMapper->truncate();
    }

    public function flushRooms()
    {
        $this->discountCodeMapper->truncate();
        $this->roomTypeMapper->truncate();
        $this->roomMapper->truncate();
    }

}