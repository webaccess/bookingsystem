<?php

class RoomBookerTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        $adapter = new \Database\PDOAdapter(DB_DRIVER . ':dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
        $this->roomTypeMapper = new \BookingSystem\Mappers\Database\RoomTypeMapper($adapter);
        $this->roomMapper = new \BookingSystem\Mappers\Database\RoomMapper($adapter, $this->roomTypeMapper);
    }

    public function tearDown()
    {
        $this->flushRooms();
    }

    public function testConstruct()
    {
        $roomManager = new \BookingSystem\Services\RoomManager($this->roomMapper, $this->roomTypeMapper);
        $this->assertInstanceOf('\BookingSystem\Services\RoomManager', $roomManager);
    }

    public function testCreateRoom()
    {
        $roomManager = new \BookingSystem\Services\RoomManager($this->roomMapper, $this->roomTypeMapper);
        $roomType = new \BookingSystem\Entities\RoomType();
        $room = $roomManager->create(1);

        $this->assertTrue($room instanceof \BookingSystem\Entities\Room);
    }  

    public function testFindAllRooms()
    {
        $roomManager = new \BookingSystem\Services\RoomManager($this->roomMapper, $this->roomTypeMapper);
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomManager->create(1);
        $roomManager->create(1);
        $roomManager->create(1);

        $this->assertEquals(3, sizeof($roomManager->findRooms()));
    }

    public function testFindRoomCapacityByType()
    {
        $roomManager = new \BookingSystem\Services\RoomManager($this->roomMapper, $this->roomTypeMapper);
        $roomType = new \BookingSystem\Entities\RoomType();
        $this->roomTypeMapper->insert($roomType);

        $roomManager->create(1);
        $roomManager->create(1);
        $roomManager->create(2);

        $this->assertEquals(2, $roomManager->findRoomCapacityByType(1));
    }

    public function flushRooms()
    {
        $rooms = $this->roomMapper->truncate();
        $rooms = $this->roomTypeMapper->truncate();
    }
}