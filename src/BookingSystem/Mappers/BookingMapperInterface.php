<?php

namespace BookingSystem\Mappers;

interface BookingMapperInterface
{
    public function findOne(array $conditions = array());
    public function findAll(array $conditions = array());
    public function insert(\BookingSystem\Entities\Booking $booking);
    public function delete($id);
}