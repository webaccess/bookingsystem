<?php

namespace BookingSystem\Mappers;

interface RoomTypeMapperInterface
{
    public function findOne(array $conditions = array());
    public function findAll(array $conditions = array());
    public function insert(\BookingSystem\Entities\RoomType $roomType);
    public function update(\BookingSystem\Entities\RoomType $roomType, $id);
    public function delete($id);
}