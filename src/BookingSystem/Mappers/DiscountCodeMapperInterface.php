<?php

namespace BookingSystem\Mappers;

interface DiscountCodeMapperInterface
{
    public function findOne(array $conditions = array());
    public function findAll(array $conditions = array());
    public function insert(\BookingSystem\Entities\DiscountCode $discountCode);
    public function delete($id);
}