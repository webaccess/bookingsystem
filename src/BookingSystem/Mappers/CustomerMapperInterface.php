<?php

namespace BookingSystem\Mappers;

interface CustomerMapperInterface
{
    public function findOne(array $conditions = array());
    public function findAll(array $conditions = array());
    public function insert(\BookingSystem\Entities\Customer $customer);
    public function delete($id);
}