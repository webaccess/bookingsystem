<?php

namespace BookingSystem\Mappers\Database;

class RoomTypeMapper extends \Database\AbstractDataMapper implements \BookingSystem\Mappers\RoomTypeMapperInterface
{
    protected $entityTable;
 
    public function __construct(\Database\DatabaseAdapterInterface $adapter)
    {
        $this->entityTable = DB_TABLES_PREFIX . 'room_types';
        parent::__construct($adapter);
    }
 
    public function insert(\BookingSystem\Entities\RoomType $roomType)
    {
        $roomType->id = $this->adapter->insert(
            $this->entityTable,
            array(
                'name' => $roomType->getName(),
                'price' => $roomType->getPrice(),
                'max_adults_number' => $roomType->getMaxAdultsNumber(),
                'max_children_number' => $roomType->getMaxChildrenNumber()
            )
        );
        return $roomType->id;
    }
    
    public function update(\BookingSystem\Entities\RoomType $roomType, $id)
    {
        $this->adapter->update(
            $this->entityTable,
            array(
                'name' => $roomType->getName(),
                'price' => $roomType->getPrice(),
                'max_adults_number' => $roomType->getMaxAdultsNumber(),
                'max_children_number' => $roomType->getMaxChildrenNumber()
            ),
            'id = ' . $id
        );
        return true;
    }

    public function delete($id)
    {
        if ($id instanceof \BookingSystem\Entities\RoomType) {
            $id = $id->getId();
        }
 
        $this->adapter->delete($this->entityTable, 'id = ' . $id);
    }

    public function truncate()
    {
        $this->adapter->prepare('TRUNCATE TABLE ' . $this->entityTable);
        $this->adapter->execute();
    }
 
    protected function createEntity(array $row)
    {
        $roomType = new \BookingSystem\Entities\RoomType();
        $roomType->setId($row['id']);
        $roomType->setName($row['name']);
        $roomType->setPrice($row['price']);
        $roomType->setMaxAdultsNumber($row['max_adults_number']);
        $roomType->setMaxChildrenNumber($row['max_children_number']);
        
        return $roomType;
    }
}