<?php

namespace BookingSystem\Mappers\Database;

class RoomMapper extends \Database\AbstractDataMapper implements \BookingSystem\Mappers\RoomMapperInterface
{
    protected $entityTable;
    protected $roomTypeMapper;
 
    public function __construct(\Database\DatabaseAdapterInterface $adapter, \BookingSystem\Mappers\RoomTypeMapperInterface $roomTypeMapper)
    {
        $this->entityTable = DB_TABLES_PREFIX . 'rooms';
        $this->roomTypeMapper = $roomTypeMapper;
        parent::__construct($adapter);
    }
 
    public function insert(\BookingSystem\Entities\Room $room)
    {
        $room->id = $this->adapter->insert(
            $this->entityTable,
            array(
                'name' => $room->getName(),
                'room_type_id' => ($room->getType()) ? $room->getType()->getId() : null,
                'available' => $room->isAvailable()
            )
        );
        return $room->id;
    }

    public function update(\BookingSystem\Entities\Room $room, $id)
    {
        $this->adapter->update(
            $this->entityTable,
            array(
                'name' => $room->getName(),
                'room_type_id' => ($room->getType()) ? $room->getType()->getId() : null,
                'available' => $room->isAvailable()
            ),
            'id = ' . $id
        );
        return true;
    }
 
    public function delete($id)
    {
        if ($id instanceof \BookingSystem\Entities\Room) {
            $id = $id->getId();
        }
 
        $this->adapter->delete($this->entityTable, 'id = ' . $id);
    }

    public function truncate()
    {
        $this->adapter->prepare('TRUNCATE TABLE ' . $this->entityTable);
        $this->adapter->execute();
    }
 
    protected function createEntity(array $row)
    {
        $roomType = $this->roomTypeMapper->findOne(array('id' => $row['room_type_id']));
        $room = new \BookingSystem\Entities\Room();
        $room->setId($row['id']);
        $room->setName($row['name']);
        $room->setType($roomType);
        $room->setAvailable($row['available']);
        
        return $room;
    }
}