<?php

namespace BookingSystem\Mappers\Database;

class BookingMapper extends \Database\AbstractDataMapper implements \BookingSystem\Mappers\BookingMapperInterface
{
    protected $entityTable;
    protected $roomMapper;
    protected $customerMapper;
    protected $discountCodeMapper;
 
    public function __construct(\Database\DatabaseAdapterInterface $adapter, \BookingSystem\Mappers\RoomMapperInterface $roomMapper, \BookingSystem\Mappers\CustomerMapperInterface $customerMapper, \BookingSystem\Mappers\DiscountCodeMapperInterface $discountCodeMapper)
    {
        $this->entityTable = DB_TABLES_PREFIX . 'bookings';
        $this->roomMapper = $roomMapper;
        $this->customerMapper = $customerMapper;
        $this->discountCodeMapper = $discountCodeMapper;
        parent::__construct($adapter);
    }

    public function insert(\BookingSystem\Entities\Booking $booking)
    {
        $booking->id = $this->adapter->insert(
            $this->entityTable,
            array(
                'room_id' => $booking->getRoom()->getId(),
                'customer_id' => ($booking->getCustomer()) ? $booking->getCustomer()->getId() : null,
                'discount_code_id' => ($booking->getDiscountCode()) ? $booking->getDiscountCode()->getId() : null,
                'start_date' => $booking->getStartDate()->format('Y-m-d'),
                'end_date' => $booking->getEndDate()->format('Y-m-d'),
                'adults_number' => $booking->getAdultsNumber(),
                'children_number' => $booking->getChildrenNumber()
            )
        );
        return $booking->id;
    }

    public function update(\BookingSystem\Entities\Booking $booking, $id)
    {
        $this->adapter->update(
            $this->entityTable,
            array(
                'room_id' => $booking->getRoom()->getId(),
                'customer_id' => ($booking->getCustomer()) ? $booking->getCustomer()->getId() : null,
                'discount_code_id' => ($booking->getDiscountCode()) ? $booking->getDiscountCode()->getId() : null,
                'start_date' => $booking->getStartDate()->format('Y-m-d'),
                'end_date' => $booking->getEndDate()->format('Y-m-d'),
                'adults_number' => $booking->getAdultsNumber(),
                'children_number' => $booking->getChildrenNumber()
            ),
            'id = ' . $id
        );
        return true;
    }
 
    public function delete($id)
    {
        if ($id instanceof \BookingSystem\Entities\Booking)
        {
            $id = $id->getId();
        }
 
        $this->adapter->delete($this->entityTable, 'id = ' . $id);
    }

    public function truncate()
    {
        $this->adapter->prepare('TRUNCATE TABLE ' . $this->entityTable);
        $this->adapter->execute();
    }
 
    protected function createEntity(array $row)
    {
        $room = $this->roomMapper->findOne(array('id' => $row['room_id']));
        $customer = $this->customerMapper->findOne(array('id' => $row['customer_id']));
        $discountCode = $this->discountCodeMapper->findOne(array('id' => $row['discount_code_id']));
        $booking = new \BookingSystem\Entities\Booking();
        $booking->setId($row['id']);
        if ($room) $booking->setRoom($room);
        if ($customer) $booking->setCustomer($customer);
        if ($discountCode) $booking->setDiscountCode($discountCode);
        $booking->setStartDate(new \DateTime($row['start_date']));
        $booking->setEndDate(new \DateTime($row['end_date']));
        $booking->setAdultsNumber($row['adults_number']);
        $booking->setChildrenNumber($row['children_number']);

        return $booking;
    }
}