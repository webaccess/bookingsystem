<?php

namespace BookingSystem\Mappers\Database;

class DiscountCodeMapper extends \Database\AbstractDataMapper implements \BookingSystem\Mappers\DiscountCodeMapperInterface
{
    protected $entityTable;
 
    public function __construct(\Database\DatabaseAdapterInterface $adapter)
    {
        $this->entityTable = DB_TABLES_PREFIX . 'discount_codes';
        parent::__construct($adapter);
    }
 
    public function insert(\BookingSystem\Entities\DiscountCode $discountCode)
    {
        $discountCode->id = $this->adapter->insert(
            $this->entityTable,
            array(
                'code' => $discountCode->getCode(),
                'percentage' => $discountCode->getPercentage(),
                'used' => $discountCode->getUsed(),
                'expiration_date' => ($discountCode->getExpirationDate()) ? $discountCode->getExpirationDate()->format('Y-m-d') : null
            )
        );
        return $discountCode->id;
    }
 
    public function delete($id)
    {
        if ($id instanceof \BookingSystem\Entities\DiscountCode) {
            $id = $id->getId();
        }
 
        $this->adapter->delete($this->entityTable, 'id = ' . $id);
    }

    public function truncate()
    {
        $this->adapter->prepare('TRUNCATE TABLE ' . $this->entityTable);
        $this->adapter->execute();
    }
 
    protected function createEntity(array $row)
    {
        $discountCode = new \BookingSystem\Entities\DiscountCode();
        $discountCode->setId($row['id']);
        $discountCode->setCode($row['code']);
        $discountCode->setPercentage($row['percentage']);
        $discountCode->setUsed($row['used']);
        $discountCode->setExpirationDate(new \DateTime($row['expiration_date']));
        
        return $discountCode;
    }
}