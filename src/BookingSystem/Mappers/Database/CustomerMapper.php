<?php

namespace BookingSystem\Mappers\Database;

class CustomerMapper extends \Database\AbstractDataMapper implements \BookingSystem\Mappers\CustomerMapperInterface
{
    protected $entityTable;
 
    public function __construct(\Database\DatabaseAdapterInterface $adapter)
    {
        $this->entityTable = DB_TABLES_PREFIX . 'customers';
        parent::__construct($adapter);
    }
 
    public function insert(\BookingSystem\Entities\Customer $customer)
    {
        $customer->id = $this->adapter->insert(
            $this->entityTable,
            array(
                'last_name' => $customer->getLastName(),
                'first_name' => $customer->getFirstName(),
                'email' => $customer->getEmail(),
                'phone' => $customer->getPhone()
            )
        );
        return $customer->id;
    }
 
    public function delete($id)
    {
        if ($id instanceof \BookingSystem\Entities\Customer) {
            $id = $id->getId();
        }
 
        $this->adapter->delete($this->entityTable, 'id = ' . $id);
    }

    public function truncate()
    {
        $this->adapter->prepare('TRUNCATE TABLE ' . $this->entityTable);
        $this->adapter->execute();
    }
 
    protected function createEntity(array $row)
    {
        $customer = new \BookingSystem\Entities\Customer();
        $customer->setId($row['id']);
        $customer->setLastName($row['last_name']);
        $customer->setFirstName($row['first_name']);
        $customer->setEmail($row['email']);
        $customer->setPhone($row['phone']);
        
        return $customer;
    }
}