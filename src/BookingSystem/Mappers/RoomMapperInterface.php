<?php

namespace BookingSystem\Mappers;

interface RoomMapperInterface
{
    public function findOne(array $conditions = array());
    public function findAll(array $conditions = array());
    public function insert(\BookingSystem\Entities\Room $room);
    public function update(\BookingSystem\Entities\Room $room, $id);
    public function delete($id);
}