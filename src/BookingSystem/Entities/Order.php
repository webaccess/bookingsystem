<?php

namespace BookingSystem\Entities;

class Order {

    private $_id;
    private $_bookings;

    public function __construct()
    {
        $this->_bookings = array();
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function addBooking(\BookingSystem\Entities\Booking $booking)
    {
        $this->_bookings[]= $booking;
    }

    public function getBookings()
    {
        return $this->_bookings;
    }

    public function getTotal()
    {
        $total = 0;
        foreach ($this->_bookings as $booking)
            $total += $booking->getTotal();

        return $total;
    }
}