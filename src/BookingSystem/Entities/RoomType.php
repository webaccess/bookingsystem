<?php

namespace BookingSystem\Entities;

class RoomType {

    private $_id;
    private $_name;
    private $_price;
    private $_maxAdultsNumber;
    private $_maxChildrenNumber;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getName()
    {
        return $this->_name;
    }
    
    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getPrice()
    {
        return $this->_price;
    }
    
    public function setPrice($price)
    {
        $this->_price = $price;
    }

    public function getMaxAdultsNumber()
    {
        return $this->_maxAdultsNumber;
    }

    public function getMaxChildrenNumber()
    {
        return $this->_maxChildrenNumber;
    }

    public function setMaxAdultsNumber($number)
    {
        $this->_maxAdultsNumber = $number;
    }

    public function setMaxChildrenNumber($number)
    {
        $this->_maxChildrenNumber = $number;
    }
}