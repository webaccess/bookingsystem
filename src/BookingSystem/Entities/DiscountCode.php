<?php

namespace BookingSystem\Entities;

class DiscountCode {

    private $_id;
    private $_code;
    private $_percentage;
    private $_used;
    private $_expirationDate;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function __construct()
    {
        $this->_code = '';
        $this->_percentage = 0;
        $this->_used = false;
        $this->_expirationDate = null;
    }

    public function getPercentage()
    {
        return $this->_percentage;
    }

    public function setPercentage($amount = 0)
    {
        if ($this->_checkPercentage($amount)) {
            $this->_percentage = $amount;
        }
    }

    public function getUsed()
    {
        return $this->_used;
    }

    public function setUsed()
    {
        $this->_used = true;
    }

    public function getDiscountAmount($amount = 0)
    {
        $discountAmount = ($this->_percentage / 100) * $amount;
        return ceil($discountAmount);
    }

    private function _checkPercentage($amount)
    {
        return ($amount >= 0 && $amount <= 100);
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function setCode($code = '')
    {
        $this->_code = $code;
    }

    public static function generateCode($length = 8)
    {
        $code = '';
        while ($length > 0) {
            $code .= self::_getRandomCharacter();
            $length--;
        }
        return $code;
    }

    private function _getRandomCharacter()
    {
        $allowedCharacters = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F');
        return $allowedCharacters[array_rand($allowedCharacters)];
    }

    public function getExpirationDate()
    {
        return $this->_expirationDate;
    }

    public function setExpirationDate($date)
    {
        $this->_expirationDate = $date;
    }

    public function checkExpirationDate()
    {
        $currentDate = new \DateTime();
        $intervalInSeconds = $this->_expirationDate->getTimestamp() - $currentDate->getTimestamp();
        
        return ($intervalInSeconds > 0);
    }
}