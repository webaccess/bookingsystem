<?php

namespace BookingSystem\Entities;

class Room {

    private $_id;
    private $_name;
    private $_type;
    private $_available;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getName()
    {
        return $this->_name;
    }
    
    public function setName($name)
    {
        $this->_name = $name;
    }

    public function getType()
    {
        return $this->_type;
    }
    
    public function setType($type)
    {
        $this->_type = $type;
    }

    public function isAvailable()
    {
        return $this->_available;
    }

    public function setAvailable($available = true)
    {
        $this->_available = $available;
    }

}