<?php

namespace BookingSystem\Entities;

class Booking {

    private $_id;
    private $_room;
    private $_startDate;
    private $_endDate;
    private $_adultsNumber;
    private $_childrenNumber;
    private $_discountCode;
    private $_customer;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getRoom()
    {
        return $this->_room;
    }

    public function setRoom(\BookingSystem\Entities\Room $room)
    {
        $this->_room = $room;
    }

    public function getStartDate()
    {
        return $this->_startDate;
    }

    public function setStartDate($startDate)
    {
        $this->_startDate = $startDate;
    }

    public function getEndDate()
    {
        return $this->_endDate;
    }

    public function setEndDate($endDate)
    {
        $this->_endDate = $endDate;
    }

    public function getAdultsNumber()
    {
        return $this->_adultsNumber;
    }

    public function setAdultsNumber($number)
    {
        $this->_adultsNumber = $number;
    }

    public function getChildrenNumber()
    {
        return $this->_childrenNumber;
    }

    public function setChildrenNumber($number)
    {
        $this->_childrenNumber = $number;
    }

    public function getNightsNumber()
    {
        if ($this->_startDate && $this->_endDate) {
            $interval = $this->_endDate->diff($this->_startDate);
            return $interval->format('%d') + 1;
        }
        throw new \Exception('The booking dates (start & end) must be defined', 1);
    }

    public function getDiscountCode()
    {
        return $this->_discountCode;
    }

    public function setDiscountCode(\BookingSystem\Entities\DiscountCode $discountCode)
    {
        $this->_discountCode = $discountCode;
    }

    public function getCustomer()
    {
        return $this->_customer;
    }

    public function setCustomer(\BookingSystem\Entities\Customer $customer)
    {
        $this->_customer = $customer;
    }

    public function getTotal()
    {
        $total = 0;
        
        if ($this->_room) {
            $total = $this->_room->getType()->getPrice() * $this->getNightsNumber();
        }

        if ($this->_discountCode)
            $total -= $this->_discountCode->getDiscountAmount($total);

        return $total;
    }

    public function checkAdultsNumber()
    {
        if ($this->_adultsNumber > $this->_room->getType()->getMaxAdultsNumber())
            return false;
        return true;
    }

    public function checkChildrenNumber()
    {
        if ($this->_childrenNumber > $this->_room->getType()->getMaxChildrenNumber())
            return false;
        return true;
    }

    public function checkRoomAvailable()
    {
        return $this->_room->isAvailable();
    }

    public function checkDates()
    {
        return $this->_endDate >= $this->_startDate;
    }

    public function startedBetween($startDate, $endDate)
    {
        return ($startDate != null) ? ($this->_startDate >= $startDate && $this->_startDate <= $endDate) : false;
    }

    public function endedBetween($startDate, $endDate)
    {
        return ($endDate != null) ? ($this->_endDate >= $startDate && $this->_endDate <= $endDate) : false;
    }

    public function startedOrEndedBetween($startDate, $endDate)
    {
        return ($this->startedBetween($startDate, $endDate) || $this->endedBetween($startDate, $endDate));
    }

    public function startedBeforeAndEndedAfter($startDate, $endDate)
    {
        return ($this->getStartDate() <= $startDate && $this->getEndDate() >= $endDate);
    }

}