<?php

namespace BookingSystem\Scenarios;

class RoomBooking {

    const BOOKING_VALIDATED = 1;
    const ERROR_ROOM_MAX_ADULTS = 2;
    const ERROR_ROOM_MAX_CHILDREN = 3;
    const ERROR_ROOM_NOT_AVAILABLE = 4;
    const ERROR_BOOKING_DATES = 5;

    public function book(\BookingSystem\Entities\Booking $booking)
    {
        if (!$booking->checkDates())
            return self::ERROR_BOOKING_DATES;

        if (!$booking->checkAdultsNumber())
            return self::ERROR_ROOM_MAX_ADULTS;

        if (!$booking->checkChildrenNumber())
            return self::ERROR_ROOM_MAX_CHILDREN;

        if (!$booking->checkRoomAvailable())
            return self::ERROR_ROOM_NOT_AVAILABLE;

        return self::BOOKING_VALIDATED;
    }
}