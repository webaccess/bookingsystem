<?php

namespace BookingSystem\Services;

class BookingManager {

    private $_roomMapper;
    private $_bookingMapper;
    private $_discountCodeMapper;

    public function __construct(\BookingSystem\Mappers\RoomMapperInterface $roomMapper, \BookingSystem\Mappers\BookingMapperInterface $bookingMapper, \BookingSystem\Mappers\DiscountCodeMapperInterface $discountCodeMapper)
    {
        $this->_roomMapper = $roomMapper;
        $this->_bookingMapper = $bookingMapper;
        $this->_discountCodeMapper = $discountCodeMapper;
    }

    public function canBook($roomType = 0, $startDateString = '', $endDateString = '', $adultsNumber = 0, $childrenNumber = 0)
    {   
        $roomId = $this->findRoomAvailableByType($roomType, $startDateString, $endDateString);
        if ($roomId) {
            $booking = $this->_createBooking($roomId, $startDateString, $endDateString, $adultsNumber, $childrenNumber);

            $roomBooking = new \BookingSystem\Scenarios\RoomBooking();
            $code = $roomBooking->book($booking);
        } else {
            $code = \BookingSystem\Scenarios\RoomBooking::ERROR_ROOM_NOT_AVAILABLE;
        }
        
        return $code;
    }

    public function book($roomType = 0, $startDateString = '', $endDateString = '', $adultsNumber = 0, $childrenNumber = 0)
    {
        $roomId = $this->findRoomAvailableByType($roomType, $startDateString, $endDateString);
        if ($roomId) {
            $booking = $this->_createBooking($roomId, $startDateString, $endDateString, $adultsNumber, $childrenNumber);

            $roomBooking = new \BookingSystem\Scenarios\RoomBooking();
            $code = $roomBooking->book($booking);

            if ($code == \BookingSystem\Scenarios\RoomBooking::BOOKING_VALIDATED) {
                return $this->_bookingMapper->insert($booking);
            }
        }

        return false;
    }

    public function getBooking($roomType = 0, $startDateString = '', $endDateString = '', $adultsNumber = 0, $childrenNumber = 0)
    {
        $roomId = $this->findRoomAvailableByType($roomType, $startDateString, $endDateString);
        if ($roomId) {
            $booking = $this->_createBooking($roomId, $startDateString, $endDateString, $adultsNumber, $childrenNumber);

            $roomBooking = new \BookingSystem\Scenarios\RoomBooking();
            $code = $roomBooking->book($booking);

            if ($code == \BookingSystem\Scenarios\RoomBooking::BOOKING_VALIDATED) {
                return $booking;
            }
        }

        return false;
    }

    public function findBookings($roomId = 0, $startDateString = '', $endDateString = '')
    {
        $criterias = ($roomId) ? array('room_id' => $roomId) : array();
        $bookings = $this->_bookingMapper->findAll($criterias);
            
        $startDate = ($startDateString) ? new \DateTime($startDateString) : null;     
        $endDate = ($endDateString) ? new \DateTime($endDateString) : null;     

        $bookingList = array();
        if ($bookings != null) {
            foreach($bookings as $i => $booking) {
                if ($booking->startedOrEndedBetween($startDate, $endDate) ||
                    $booking->startedBeforeAndEndedAfter($startDate, $endDate) ||
                    ($startDate == null && $endDate == null)
                ) {
                    $bookingList[]= $booking;
                }
            }
        }
        
        return $bookingList;
    }

    public function findRoom($roomId)
    {
        return $this->_roomMapper->findOne(array('id' => $roomId));
    }

    private function _createBooking($roomId = 0, $startDateString = '', $endDateString = '', $adultsNumber = 0, $childrenNumber = 0)
    {
        $booking = new \BookingSystem\Entities\Booking();
        $startDate = new \DateTime($startDateString);
        $endDate = new \DateTime($endDateString);

        $booking->setStartDate($startDate);
        $booking->setEndDate($endDate);

        $booking->setAdultsNumber($adultsNumber);
        $booking->setChildrenNumber($childrenNumber);
        
        $room = $this->findRoom($roomId);

        $room->setAvailable(true);
        $booking->setRoom($room);

        return $booking;
    }

    public function isRoomAvailable($roomId, $startDateString, $endDateString)
    {
        $bookings = $this->findBookings($roomId, $startDateString, $endDateString);

        return (sizeof($bookings) > 0) ? false : true;
    }

    public function findRoomAvailableByType($type, $startDateString, $endDateString)
    {
        $rooms = $this->_roomMapper->findAll(array('room_type_id' => $type, 'available' => 1));

        if ($rooms != null) {
            foreach ($rooms as $room) {
                if ($this->isRoomAvailable($room->getId(), $startDateString, $endDateString)) {
                    return $room->getId();
                }
            }
        }
        return false;
    }

    public function findDiscountCode($code)
    {
        $discountCode = $this->_discountCodeMapper->findOne(array('code' => strtoupper($code)));

        if ($discountCode != null) {
            return $discountCode;
        }
        return false;
    }

}