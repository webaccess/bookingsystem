<?php

namespace BookingSystem\Services;

class CustomerManager {

    private $_customerMapper;

    public function __construct(\BookingSystem\Mappers\CustomerMapperInterface $customerMapper)
    {
        $this->_customerMapper = $customerMapper;
    }
    
    public function getCustomer($lastName, $firstName, $email, $phone)
    {
        $customer = new \BookingSystem\Entities\Customer();
        $customer->setLastName($lastName);
        $customer->setFirstName($firstName);
        $customer->setEmail($email);
        $customer->setPhone($phone);

        return $customer;
    }

    public function insert(\BookingSystem\Entities\Customer $customer)
    {
        $customerId = $this->_customerMapper->insert($customer);
        $customer->setId($customerId);
        
        return $customer;
    }

}