<?php

namespace BookingSystem\Services;

class RoomManager {

    private $_roomMapper;

    public function __construct(\BookingSystem\Mappers\RoomMapperInterface $roomMapper, \BookingSystem\Mappers\RoomTypeMapperInterface $roomTypeMapper)
    {
        $this->_roomMapper = $roomMapper;
        $this->_roomTypeMapper = $roomTypeMapper;
    }

    public function create($type = null)
    {
        $roomType = $this->_roomTypeMapper->findOne(array('id' => $type));
        $room = new \BookingSystem\Entities\Room();
        $room->setType($roomType);
        $room->setAvailable(1);
        $this->_roomMapper->insert($room);

        return $room;
    }

    public function findRooms()
    {
        $rooms = $this->_roomMapper->findAll();

        return $rooms;
    }

    public function findRoomCapacityByType($type)
    {
        $rooms = $this->_roomMapper->findAll(array('room_type_id' => $type));
        return ($rooms != null) ? sizeof($rooms) : 0;
    }
}